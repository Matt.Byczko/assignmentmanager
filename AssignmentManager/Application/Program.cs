﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManager.Application
{
    class Program
    {
        public enum Importance
        {
            None,
            Low,
            Medium,
            High
        }
        static void Main(string[] args)
        {
            Task task1 = new Task("initial task", Importance.High);
            Task task2 = new Task("initial task", Importance.Low);
            Task task3 = new Task("initial task", Importance.High, System.DateTime.Now);

            Console.WriteLine(task1);
            Console.WriteLine(task2);
            Console.WriteLine(task3);
            Console.ReadLine();
        }
    }
}
