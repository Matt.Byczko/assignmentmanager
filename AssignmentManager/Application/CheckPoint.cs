﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskManager.Application;

namespace AssignmentManager.Application
{
    class CheckPoint
    {
        private String name;
        private String description;
        private System.DateTime dueDate;
        private Program.Importance importanceRating;
        public CheckPoint(String newName, String description, System.DateTime dueDate, Program.Importance rating)
        {
            this.name = newName;
            this.description = description;
            this.dueDate = dueDate;
            this.importanceRating = rating;
        }

        public string Description()
        {
            return description;
        }

        public void SetDescription(String value)
        {
            description = value;
        }

        public DateTime DueDate()
        {
            return dueDate;
        }

        public void SetDueDate(DateTime value)
        {
            dueDate = value;
        }
        
        public string Name()
        {
            return name;
        }

        internal Program.Importance ImportanceRating()
        {
            return importanceRating;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
