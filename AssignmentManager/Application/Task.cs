﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssignmentManager.Application;

namespace TaskManager.Application
{
    /// <summary>
    /// This class will play the role of a task to perform in a task manager.
    /// It will hold important details such as due date, details and taskname.
    /// </summary>
    class Task
    {
        private String taskName;
        private String taskDescription;
        private System.DateTime dueDate;
        private System.DateTime creationDate;
        private Program.Importance importanceRating;
        private Dictionary<String, CheckPoint> checkpoints = new Dictionary<String, CheckPoint>();
        //private List<CheckPoint> notes = new List<CheckPoint>();

        public Task(String taskName, Program.Importance rating)
        {
            this.taskName = taskName;
            creationDate = System.DateTime.Now;
            dueDate = System.DateTime.MaxValue;
            importanceRating = rating;
        }
        
        public Task(String taskName, Program.Importance rating, System.DateTime dueDate)
        {
            this.taskName = taskName;
            creationDate = System.DateTime.Now;
            this.dueDate = dueDate;
            importanceRating = rating;
        }

        public bool DeleteCheckPoint(String name)
        {
            return checkpoints.Remove(name);
        }

        public void UpdateCheckPoint(String name, CheckPoint updatedPoint)
        {
            checkpoints.Remove(name);
            checkpoints.Add(name, updatedPoint);
        }

        public CheckPoint RetrieveCheckPoint(String name)
        {
            return checkpoints[name];
        }

        public bool CreateCheckPoint(String name,String description, System.DateTime dueDate,Program.Importance rating)
        {
            if (checkpoints.ContainsKey(name))
            {
                return false;
            }
            checkpoints.Add(name, new CheckPoint(name, description, dueDate, rating));
            return true;
        }

        public String GetName()
        {
            return taskName;
        }

        public void SetDescription(String newDesc)
        {
            taskDescription = newDesc;
        }

        public String GetDescription()
        {
            return taskDescription;
        }

        public System.DateTime GetCreationDate()
        {
            return creationDate;
        }

        public System.DateTime GetDueDate()
        {
            return dueDate;
        }

        public Program.Importance GetImportance()
        {
            return importanceRating;
        }

        public void SetDueDate(System.DateTime newDate)
        {
            dueDate = newDate;
        }

        public override string ToString()
        {
            return String.Concat("Taskname: ", taskName, "Created on: ", creationDate, "Due on: ", dueDate, "Importance: ", (importanceRating == Program.Importance.High) ? "high" : "not high");
        }

    }
}