﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskManager.Application;
using AssignmentManager.Application;

namespace UnitTestAssignmentManager
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAccessorsTask()
        {
            Task t1 = new Task("name", Program.Importance.High);
            t1.SetDescription("description here");
            Assert.AreEqual(t1.GetName(), "name");
            Assert.AreEqual(t1.GetImportance(), Program.Importance.High);
            Assert.AreEqual(t1.GetDescription(), "description here");
            Assert.IsNotNull(t1);
        }

        [TestMethod]
        public void TestCheckPoint()
        {
            Task t1 = new Task("name", Program.Importance.High);
            t1.SetDescription("description here");
            t1.CreateCheckPoint("name1", "do stuff", System.DateTime.Now, Program.Importance.High);

            Assert.AreEqual(t1.RetrieveCheckPoint("name1").Name(), "name1");
            Assert.AreEqual(t1.RetrieveCheckPoint("name1").Description(), "do stuff");
            CheckPoint temp = t1.RetrieveCheckPoint("name1");
            temp.SetDescription("newDesc");
            t1.UpdateCheckPoint("name1", temp);

            Assert.AreEqual(t1.RetrieveCheckPoint("name1").Description(), "newDesc");
        }

    }
}
